# COVID-19

Naam: Stephan van der Zwaard 

Datum: 23-05-2020

### Summary

Visualize the development of COVID-19 in the Netherlands. 

Data is obtained from the [RIVM](https://www.rivm.nl/coronavirus-covid-19/grafieken), [CBS](https://www.cbs.nl/en-gb/news/2020/20/nearly-9-thousand-more-deaths-in-first-9-weeks-of-covid-19), and [NICE](https://stichting-nice.nl/covid-19-op-de-ic.jsp). 

Daily numbers are presented for the confirmed cases, hospitalized patients, patients that were admitted to the intensive care unit (ICU), deaths and excess deaths. 

It should be noted that testing capacity in the Netherlands was limited, which likely explains the limited number of confirmed cases and the high number of 'excess deaths' (i.e. in excess of what is predicted based on seasonal mortality within the Netherlands in the previous years): 

- On 2020-03-20 RIVM announced that 'the actual number of infections with COVID-19 is higher than those reported from this update and onwards, because not everyone with potential infection is tested any more.

- On 2020-03-25, GPs report that the number of deaths due to Coronavirus is also much higher since GPs have no way of reporting statistics on deaths of suspected Corona patients without having them tested.

### Repository

The data and script that are used to create the visualization are stored in the [Gitlab repository](https://gitlab.com/svdzwaard/covid-19/).

### Data 

Open data was retrieved to visualize the development of COVID-19 in the Netherlands and was accessed on the 22th of May.

Data on confirmed cases, hospitalized patients and deaths were obtained from:

- [RIVM](https://www.rivm.nl/coronavirus-covid-19/grafieken)

Data on intensive care unit was obtained from:

- [NICE](https://stichting-nice.nl/covid-19-op-de-ic.jsp). 

Data on excess mortality was obtained from:

- [CBS](https://www.cbs.nl/en-gb/news/2020/20/nearly-9-thousand-more-deaths-in-first-9-weeks-of-covid-19)

- [Dutchnews on excess mortality](https://www.dutchnews.nl/news/2020/05/dutch-excess-mortality-rate-disappears-as-covid-19-impact-declines/)

Data on key events and mitigation measures were obtained from:

- [COVID-19 pandemic in the Netherlands](https://en.wikipedia.org/wiki/COVID-19_pandemic_in_the_Netherlands)

- [Mitigation measures](https://www.kaggle.com/paultimothymooney/covid19-containment-and-mitigation-measures/data)