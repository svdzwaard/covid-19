# -------------------------- COVID-19 plot --------------------------------------------------
#
# Omschrijving: Create visualization of progression of COVID-19 in the Netherlands
# Auteurs:      Stephan van der Zwaard
# Datum:        22-05-2020
# Versie:       1.0

# --------------------------------- Prerequisites -------------------------------------------

# Set options
options(stringsAsFactors = F)

# Load required libraries
library(httr);       library(jsonlite)
library(lubridate);  library(tidyverse)
library(extrafont);  library(nlraa)
library(xkcd);       library(MASS)
library(nlme);       library(stats)

# ---------------------------------- Data acquisition ----------------------------------------

# Daily IC data (see also: https://www.databronnencovid19.nl/Bron?naam=Nationale-Intensive-Care-Evaluatie and https://www.stichting-nice.nl/covid-19-op-de-ic.jsp)
daily_ic_confirmed    <- fromJSON("https://stichting-nice.nl/covid-19/public/new-intake/")[[1]] 
daily_ic_suspect      <- fromJSON("https://stichting-nice.nl/covid-19/public/new-intake/")[[2]] 
total_ic_confirmed    <- fromJSON("https://stichting-nice.nl/covid-19/public/intake-count/")

# Daily confirmed cases
daily_cases_confirmed <- read.csv("data/ggd-patients.csv", sep=";")

# Daily hospitalization 
daily_hospitalized    <- read.csv("data/hospitalized-patients.csv", sep=";")

# Daily deaths
daily_deaths          <- read.csv("data/deceased-patients.csv", sep=";")

# Weekly excess mortality
weekly_excess_deaths  <- read.csv("data/deaths-registered-weekly.csv", sep=",")

#Containment measures per country
counter_measures      <- read.csv("data/containment_measures.csv", sep=",")


# ---------------------------------- Data preprocessing ----------------------------------------

daily_ic              <- daily_ic_confirmed %>% mutate(value = value + daily_ic_suspect$value) %>%
                         filter(date != "2020-05-22") %>% 
                         mutate(date = as.Date(date)) %>%
                         rename(ic_intake = value)

daily_cases_confirmed <- daily_cases_confirmed %>% 
                         mutate(cases = tot.en.met.gisteren + nieuw,
                                date  = as.Date(GGD.meldingsdatum, "%d/%b")) %>% 
                         select(date,cases)

daily_hospitalized    <- daily_hospitalized %>% 
                         mutate(hospitalized = tot.en.met.gisteren + nieuw,
                                date         = as.Date(Datum.ziekenhuisopname, "%d/%b")) %>% 
                         select(date,hospitalized)

daily_deaths          <- daily_deaths %>% 
                         mutate(deaths = tot.en.met.gisteren + nieuw,
                                date   = as.Date(Datum.van.overlijden, "%d/%b")) %>%  
                         select(date,deaths)

weekly_excess_deaths  <- weekly_excess_deaths %>%
                         mutate(date          = parse_date_time(paste(2020, week, 'Mon', sep="/"),'Y/W/a'),
                                excess_deaths = (as.numeric(X2020.) - as.numeric(X2020..verwachting.))/7) %>%
                         filter(!is.na(excess_deaths)) %>% select(date,excess_deaths) %>%
                         mutate(date = as.Date(date))

daily_data            <- daily_cases_confirmed %>% select(date,cases) %>%
                         left_join(daily_hospitalized, by=c("date")) %>%
                         left_join(daily_ic, by=c("date")) %>%
                         left_join(daily_deaths, by=c("date")) %>%
                         left_join(weekly_excess_deaths, by=c("date")) %>%
                         gather(type,value,-date)

# ---------------------------------- Fitted curves ----------------------------------------

# Create function for fitting curves
fit_model <- function(data,curve) {
  
  daily_dat    <- data %>% filter(type == curve)
  dat          <- data.frame(x = as.numeric(daily_dat$date-daily_dat$date[1]), y=daily_dat$value)
  fitted_model <- nls(y ~ SSbell(x, ymax, a, b, xc), data = dat)
  
  return(fitted_model)
}

# Create fitted curves
fit_cases         <- fit_model(daily_data,"cases")
fit_deaths        <- fit_model(daily_data,"deaths")
fit_hospital      <- fit_model(daily_data,"hospitalized")
fit_ic            <- fit_model(daily_data,"ic_intake")
fit_excess_deaths <- fit_model(daily_data,"excess_deaths")

daily_data <- daily_data %>% mutate(type = gsub("_","-",type)) %>%
                             mutate(type = case_when(type=="cases" ~"confirmed-cases",
                                                     type=="ic-intake" ~ "icu-intake",
                                                     TRUE ~ type)) %>%
                             mutate(type = factor(type, levels = c("confirmed-cases","hospitalized","icu-intake","deaths","excess-deaths")))

fitted_cases <- daily_data %>% filter(type == "confirmed-cases") %>% mutate(fitted = fitted(fit_cases))


# ---------------------------------- Data visualization ----------------------------------------

# Download and load the XKCD-font
xkcdFontURL <- "http://simonsoftware.se/other/xkcd.ttf"
download.file(xkcdFontURL,dest="xkcd.ttf",mode="wb")
font_import(".")   ## because we downloaded to working directory
loadfonts()

# Make plot
p <- ggplot(daily_data, aes(x=date, y=value, group=type, color=type)) +
  
        # Plot all data points
        geom_point(size=.1) +
  
        # Plot bell-shaped curves
        geom_line(data = daily_data   %>% filter(type == "confirmed-cases"), aes(y = fitted(fit_cases)), linetype = 1, size = 2) +
        geom_line(data = daily_data   %>% filter(type == "hospitalized"),    aes(y = fitted(fit_hospital)), linetype = 1, size = 2)  +
        geom_line(data = daily_data   %>% filter(type == "excess-deaths",    !is.na(value)), aes(y = fitted(fit_excess_deaths)), linetype = 2, size = 1.2)+
        geom_line(data = daily_data   %>% filter(type == "deaths"),          aes(y = fitted(fit_deaths)), linetype = 1, size = 2)  +
        geom_ribbon(data = daily_data %>% filter(type == "icu-intake"),       aes(ymin = 0, ymax = fitted(fit_ic)), fill = "#f2cfcf", alpha = .75, show.legend=FALSE) +
        geom_line(data = daily_data   %>% filter(type == "icu-intake"),       aes(y = fitted(fit_ic)), linetype = 1, size = 2)  +
  
        # Add datapoints of key events
        geom_point(aes(x= as.Date("2020-02-27"),y=10),  color="#34393c", size=3) +
        geom_point(aes(x= as.Date("2020-03-06"),y=71),  color="#34393c", size=3) +
        geom_point(aes(x= as.Date("2020-03-15"),y=322), color="#34393c", size=3) +
        geom_point(aes(x= as.Date("2020-03-16"),y=366), color="#34393c", size=3) +
        geom_point(aes(x= as.Date("2020-03-23"),y=727), color="#34393c", size=3) +
        geom_point(aes(x= as.Date("2020-03-29"),y=1010),color="#34393c", size=3) +
        geom_point(aes(x= as.Date("2020-03-31"),y=1075),color="#34393c", size=3) +
        geom_point(aes(x= as.Date("2020-04-05"),y=1150),color="#34393c", size=3) +
        geom_point(aes(x= as.Date("2020-04-07"),y=1145),color="#34393c", size=3) +
        geom_point(aes(x= as.Date("2020-04-13"),y=1020),color="#34393c", size=3) +
        geom_point(aes(x= as.Date("2020-04-22"),y=691), color="#34393c", size=3) +
        geom_point(aes(x= as.Date("2020-05-02"),y=375), color="#34393c", size=3) +
        geom_point(aes(x= as.Date("2020-05-03"),y=351), color="#34393c", size=3) +
        geom_point(aes(x= as.Date("2020-05-11"),y=208), color="#34393c", size=3) +

        # Add text of key events
        annotate("text",x=as.Date("2020-03-12"),y=215,  label="★",                                   size=7, color ="#34393c", family = "HiraKakuPro-W3") +
        annotate("text",x=as.Date("2020-03-04"),y=270,  label="Start lockdown",                       size=3.4, color ="#be1111", family = "xkcd") +
        annotate("text",x=as.Date("2020-03-01"),y=185,  label="first death",                          size=3.4, color ="#34393c", family = "xkcd") +
        annotate("text",x=as.Date("2020-02-27"),y=100,  label="first case",                           size=3.4, color ="#34393c", family = "xkcd") +
        annotate("text",x=as.Date("2020-03-07"),y=330,  label="speech Rutte",                         size=3.4, color ="#34393c", family = "xkcd") +
        annotate("text",x=as.Date("2020-03-08"),y=390,  label="schools closed",                       size=3.4, color ="#34393c", family = "xkcd") +
        annotate("text",x=as.Date("2020-03-13"),y=800,  label="stricter social",                      size=3.4, color ="#34393c", family = "xkcd") +
        annotate("text",x=as.Date("2020-03-13"),y=750,  label="distancing rules",                     size=3.4, color ="#34393c", family = "xkcd") +
        annotate("text",x=as.Date("2020-03-14"),y=1050, label="10.000 confirmed cases",               size=3.4, color ="#34393c", family = "xkcd") +
        annotate("text",x=as.Date("2020-03-14"),y=1000, label="-actual numbers are higher,",          size=3.4, color ="#34393c", family = "xkcd") +
        annotate("text",x=as.Date("2020-03-14"),y=950,  label="because of limited testing",           size=3.4, color ="#34393c", family = "xkcd") +
        annotate("text",x=as.Date("2020-03-21"),y=1200, label="Corona patients on ICU",               size=3.4, color ="#34393c", family = "xkcd") +
        annotate("text",x=as.Date("2020-03-21"),y=1150, label="exceeded normal capacity",             size=3.4, color ="#34393c", family = "xkcd") +
        annotate("text",x=as.Date("2020-04-05"),y=1360, label="ICU capacity doubled to",              size=3.4, color ="#34393c", family = "xkcd") +
        annotate("text",x=as.Date("2020-04-05"),y=1300, label="2400 in past 2 weeks",                 size=3.4, color ="#34393c", family = "xkcd") +
        annotate("text",x=as.Date("2020-04-10"),y=1200, label="peak in total Corona patients on ICU", size=3.4, color ="#34393c", family = "xkcd", hjust = 0) +
        annotate("text",x=as.Date("2020-04-24"),y=1100, label="peak in excess deaths,",               size=3.4, color ="#34393c", family = "xkcd") +
        annotate("text",x=as.Date("2020-04-24"),y=1050, label="due to limited testing",               size=3.4, color ="#34393c", family = "xkcd") +
        annotate("text",x=as.Date("2020-05-11"),y=350,  label="elem. schools reopened",               size=3.4, color ="#34393c", family = "xkcd", hjust = 0) +
        annotate("text",x=as.Date("2020-05-11"),y=295,  label="'contact jobs' resumed",               size=3.4, color ="#34393c", family = "xkcd", hjust = 0) +
        annotate("text",x=as.Date("2020-05-05"),y=800,  label="countermeasures extended",             size=3.4, color ="#34393c", family = "xkcd") +
        annotate("text",x=as.Date("2020-05-05"),y=750,  label="to 20th of May",                       size=3.4, color ="#34393c", family = "xkcd") +
        annotate("text",x=as.Date("2020-05-02"),y=500,  label="corona patients on icu halved",        size=3.4, color ="#34393c", family = "xkcd", hjust = 0) +
        annotate("text",x=as.Date("2020-05-05"),y=425,  label="5.000 deaths",                         size=3.4, color ="#34393c", family = "xkcd", hjust = 0) +
        annotate("text",x=as.Date("2020-05-15"),y=220,  label="9.000 excess deaths",                  size=3.4, color ="#34393c", family = "xkcd", hjust = 0) +
        annotate("text",x=as.Date("2020-06-01"),y=850,  label="Stephan van der Zwaard",               size=3.1, color ="#999c9d", family = "xkcd", hjust = .5, angle=90) +
        annotate("text",x=as.Date("2020-04-06"),y=650,  label="covid-19",                             size=8, color ="#be1111", family = "xkcd") +
  
        # Add lines to key event
        geom_segment(data=data.frame(x    = as.Date(c("2020-03-04","2020-03-20","2020-03-24","2020-03-28","2020-04-05","2020-04-08","2020-04-14","2020-04-23","2020-05-02","2020-05-04","2020-05-11","2020-05-12")),
                                     y    = c(140,760,1035,1110,1190,1165,1040,710,410,370,210,210),
                                     xend = as.Date(c("2020-03-05","2020-03-22","2020-03-28","2020-03-30","2020-04-05","2020-04-09","2020-04-15","2020-04-26","2020-05-03","2020-05-05","2020-05-12","2020-05-14")),
                                     yend = c(100,740,1010,1085,1250,1190,1065,760,465,390,250,210),
                                     type = NA), mapping=aes(x=x, y=y, xend=xend, yend=yend), size=.5, color="#34393c") +

        # Add layout
        geom_vline(xintercept=as.Date("2020-02-21"),size=1.1) +
        geom_hline(yintercept=0, size=1.2) +
        ggtitle("Intelligent lockdown in the Netherlands: \n did we flatten the curve?") +
        labs(y="No. of people per day\n", x="", color = "") +
        scale_color_manual(values = c("#de8888","#BE1111","#850b0b","#34393c","#999c9d")) +
        scale_x_date(date_breaks = "months" , date_labels = "%b-%y", limits = as.Date(c("2020-02-25","2020-06-01"))) +
        scale_y_continuous(breaks=c(0,500,1000,1500), limits = c(0,1500)) +
        xkcd::theme_xkcd() + 
        theme(plot.title = element_text(size=23,hjust=.5),
              legend.position = "bottom")

  
# Save plot
  png(file=paste0("covid19_NL.png"), width=228,height=163,units = "mm", bg = "white", res = 200, type = "quartz")
  p
  dev.off()

